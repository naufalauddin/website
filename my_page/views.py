from django.shortcuts import render

# Create your views here.
def front_page(request):
    return render(request, 'my_page/front_page.html', {'title':'Naufal'})

def about_me(request):
    return render(request, 'my_page/about_me.html', {'title': 'Naufal'})

def skill(request):
    return render(request, 'my_page/skill.html', {'title': 'Naufal'})

def experiment(request):
    return render(request, 'my_page/experiment.html', {'title': 'Experimental Works'})

def contacts(request):
    return render(request, 'my_page/contacts.html', {'title': 'Find ME'})
