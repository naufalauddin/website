from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from .forms import JadwalForm
from .models import Jadwal

DAYS = [
        'Senin',
        'Selasa',
        'Rabu',
        'Kamis',
        'Jumat',
        'Sabtu',
        'Minggu'
        ]
# Create your views here.
def daftar(request):
    jadwals = Jadwal.objects.order_by('date')
    for jadwal in jadwals:
        jadwal.day = DAYS[jadwal.date.weekday()]
    return render(request, 'jadwal/base.html', { 'title': 'jadwal', 'jadwals': jadwals})

def buat(request):
    if request.method == 'POST':
        form = JadwalForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            date = form.cleaned_data['date']
            time = form.cleaned_data['time']
            category = form.cleaned_data['category']
            place = form.cleaned_data['place']

            jadwal = Jadwal(name=name, date=date, time=time, place=place, category=category)
            jadwal.save()

            return HttpResponseRedirect(reverse('jadwal:daftar'))

    else:
        form = JadwalForm()
    return render(request, 'jadwal/buat.html', {'form': form})

def delete(request, id):
    Jadwal.objects.get(pk=id).delete()
    return HttpResponseRedirect(reverse('jadwal:daftar'))
