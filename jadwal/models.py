from django.db import models

# Create your models here.
class Jadwal(models.Model):
    name = models.CharField(max_length=200)
    date = models.DateField()
    time = models.TimeField()
    place = models.CharField(max_length=200)
    category = models.CharField(max_length=200)
