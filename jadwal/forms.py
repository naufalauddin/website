from django import forms
from .models import Jadwal
from datetime import date

TEXT_ATTRS = {
        'size': 50,
        'style': 'font-family: montserrat; border-radius: 20px; height: 20px; padding: 5px'
    }

class JadwalForm(forms.Form):
    name = forms.CharField(label='Nama Kegiatan', max_length=200, widget=forms.TextInput(attrs=TEXT_ATTRS))
    date = forms.DateField(label='Tanggal', widget=forms.DateInput(attrs={'type': 'date', 'min':date.today(), 'style':'border-radius: 20px; padding: 5px'}))
    time = forms.TimeField(label='Jam', widget=forms.TimeInput(attrs={'type':'time', 'style':'border-radius: 20px; padding: 5px'}))
    place = forms.CharField(label='Tempat', max_length=200, widget=forms.TextInput(attrs=TEXT_ATTRS))
    category = forms.CharField(label='Kategori', max_length=200, widget=forms.TextInput(attrs=TEXT_ATTRS))
