from django.urls import path

from . import views

app_name = 'jadwal'

urlpatterns = [
    path('', views.daftar, name='daftar'),
    path('buat', views.buat, name='buat'),
    path('del/<int:id>', views.delete, name='del'),
]
